// ##########################
// Dependancies
// ##########################
var SerialPort = require("serialport");
var Readline = require('@serialport/parser-readline');
var axios = require('axios');

// ##########################
// Constants and Variables
// ##########################
// configuration constants
const pollingFrequency = 0.5; //in minutes
const sensorSerialPort = '/dev/ttyUSB0';
// const sensorSerialPort = 'COM5';
const sensorBaudRate = 9600;
const serverURL = 'https://nvxz8aeda6.execute-api.us-west-2.amazonaws.com/Test/add-reading';

// ##########################
// Serial Port Setup
// ##########################
SerialPort.list(function (err, ports) {
    console.log('Current Ports:');
    ports.forEach(function(port) {
      console.log(port.comName);
  });
});

const parser = new Readline();
var port = new SerialPort(sensorSerialPort, {
  baudRate: sensorBaudRate,
});
port.pipe(parser);

// ##########################
// Serial Port Events
// ##########################
// when serial port first opens
port.on('open', function() {
  console.log ('Serial Port Opened');
  console.log ('Starting Polling...');
  //port.write('V');  //arduino is not yet ready since it resets on new serial connection

  // start polling for data
  setInterval(function(){
    console.log('writing to port');
    port.write('V');
  }, 60000 * pollingFrequency);
});

// when new data is incomming from serial port
parser.on('data', function(data) {
  if (data[0] = 'V') {
    updateLog(parseFloat(data.substr(1)));
    console.log(data.substring(1));
  }
});

// when serial port encounters an error
parser.on('error', function(err) {
  if (err.message.indexOf('cannot open /dev/ttyUSB0') > -1) {
    console.log('Could not connect to USB Voltmeter');
  }
  console.log('Error: ', err.message);
});

// ##########################
// Helper Functions
// ##########################
function updateLog (voltage) {
    var currentDate = new Date();
    var reading = '[' + currentDate.valueOf() + ',' + voltage + ']'
    axios.post(serverURL, {
        date: currentDate.today(),
        reading: reading
    })
    .then(response => {
        console.log(response.data);
    })
}

// For todays date;
Date.prototype.today = function () {
    return (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+ this.getFullYear();
    //return ((this.getDate() < 10)?"0":"") + this.getDate() +"-"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+ this.getFullYear();
}
