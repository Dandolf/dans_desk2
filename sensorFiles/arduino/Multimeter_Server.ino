
void setup() {
  Serial.begin(115200);
}

void loop() {
  String data = "";
  while (Serial.available()) {
    data = Serial.readString();
  }

  if (data == "V") {
    float voltage = getVoltage();
    Serial.print("V");
    Serial.println(voltage);
  }
}

float getVoltage() {
  int samples = 10;
  int data[samples];

  for (int i = 0; i < samples; i++) {
    data[i] = analogRead(A7);
    delay(5);
  }

  int average = 0;

  for (int i=0; i < samples; i++) {
    average += data[i];
  }
  average /= samples;
  return (average * 30.78) / 1024.0;
}
