import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

// Import app modules
import SolarDesk from './storeModules/SolarDesk'

export default new Vuex.Store({
	modules: {
		SolarDesk: SolarDesk
	},
	state: {},
	mutations: {},
	actions: {},
	getters: {}
})
