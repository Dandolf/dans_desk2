import axios from 'axios'

export default {
    namespaced: true,
    state: {
		solarDesk: {
			dateToView: null,
			currentData: [],
			awsURL: 'https://nvxz8aeda6.execute-api.us-west-2.amazonaws.com/Test'
		}
	},
	mutations: {
		updateSolarData (state, payload) {
			if (payload.hasOwnProperty('Item')) {
				state.solarDesk.currentData = JSON.parse(payload.Item.Readings.S)
			} else {
				state.solarDesk.currentData = []
			}
		}
	},
	actions: {
		getSolarData (context, payload) {
			return axios({
				method: 'post',
				url: context.getters.solarDesk.awsURL + '/get-readings',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					date: payload
				}
			})
			.then(response => {
				context.commit('updateSolarData', response.data)
				return response.data
			})
		}
	},
	getters: {
		solarDesk (state) {
			return state.solarDesk
		}
	}
}